package org.cbrnwatchboard.injects.essence;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Iterator;

/**
 * Created by gdonarum on 8/24/2016.
 */
public class ChiefComplaintGenerator {
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final SimpleDateFormat date = new SimpleDateFormat("M/d/yyyy");
    private static final SimpleDateFormat dateTime = new SimpleDateFormat("M/d/yyyy h:mm");


    public void run(String inputFolderName, String outputFileName) {
        File folder = new File(inputFolderName);
        File[] listOfFiles = folder.listFiles();
        String out = "\"RowID\",\"Date\",\"LastModifiedDate\",\"SendingFacility\",\"Hospital\",\"FacilityID\",\"MedRecNo\",\"ChiefComplaintOrig\",\"Age\",\"MessageDateTime\",\"Time\",\"ZipCode\",\"Sex\"\n";
        int recordNo = 1;
        for(File file : listOfFiles) {
            String json = readFileAsString(inputFolderName+"/"+file.getName());
            //System.out.println(json);
            try {
                ChiefComplaintRun run = (ChiefComplaintRun) mapper.readValue(json, ChiefComplaintRun.class);
                //System.out.println(run.getType());
                for (int i = 0; i < run.getCount(); i++) {
                    // RowId
                    out += "" + recordNo;
                    // Date
                    out += ",\"" + date.format(run.getDate()) + "\"";
                    // LastModifiedDate
                    out += ",\"" + dateTime.format(run.getDate()) + "\"";
                    // hospital
                    //System.out.println("zips: " + run.getZipcodes().size());
                    Zipcode zip = getZipcode(Math.random(), run);
                    out += ",\"" + zip.getHospital() + "\"";
                    out += ",\"" + zip.getHospital() + "\"";    // SendingFacility
                    // FacilityID
                    out += ",\"" + zip.getHospital() + "\"";
                    // MedRecNo
                    out += ",\"" + zip.getHospital().replaceAll(" ", "") + "_AER_" + recordNo + "\"";
                    // ChiefComplaintOrig
                    out += ",\"" + getComplaint(Math.random(), run).getText() + "\"";
                    // Age
                    int plus = (int) Math.round(Math.random() * (run.getMaxAge() - run.getMinAge()));
                    int age = plus + run.getMinAge();
                    out += "," + age;
                    // MessageDateTime
                    out += ",\"" + dateTime.format(run.getDate()) + "\"";
                    // Time
                    out += ",\"" + dateTime.format(run.getDate()) + "\"";
                    // ZipCode
                    out += ",\"" + zip.getCode() + "\"";
                    // Sex
                    int percent = (int) Math.round(Math.random() * 100);
                    String sex = "M";
                    if (percent > run.getPercentMale()) {
                        sex = "F";
                    }
                    out += ",\"" + sex + "\"";
                    // end
                    out += "\n";
                    recordNo++;
                }
                System.out.println(out);
                PrintWriter outFile = new PrintWriter(outputFileName);
                outFile.println(out);
                outFile.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Zipcode getZipcode(double seed, ChiefComplaintRun run) {
        Iterator<Zipcode> it = run.getZipcodes().iterator();
        int distribution = 0;
        int distTarget = (int)Math.round(100*seed);
        Zipcode zip = null;
        while(it.hasNext()) {
            zip = it.next();
            int bottomDist = distribution;
            distribution+=zip.getDistribution();
            //System.out.println(bottomDist + " - " + distTarget + " - " + distribution);
            if((distTarget>=bottomDist) && (distTarget<distribution)) {
                return zip;
            }
        }
        return zip;
    }

    private Complaint getComplaint(double seed, ChiefComplaintRun run) {
        Iterator<Complaint> it = run.getComplaints().iterator();
        int distribution = 0;
        int distTarget = (int)Math.round(100*seed);
        Complaint complaint = null;
        while(it.hasNext()) {
            complaint = it.next();
            int bottomDist = distribution;
            distribution+=complaint.getDistribution();
            //System.out.println(bottomDist + " - " + distTarget + " - " + distribution);
            if((distTarget>=bottomDist) && (distTarget<distribution)) {
                return complaint;
            }
        }
        return complaint;
    }

    private String readFileAsString(String fileName) {
        try {
            InputStream inputStream       = new FileInputStream(fileName);
            Reader      reader = new InputStreamReader(inputStream);
            StringBuffer fileData = new StringBuffer(1000);
            char[] buf = new char[1024];
            int numRead = 0;
            while ((numRead = reader.read(buf)) != -1) {
                String readData = String.valueOf(buf, 0, numRead);
                fileData.append(readData);
                buf = new char[1024];
            }
            reader.close();
            return fileData.toString();
        } catch(Exception e) {

        }
        return "ERROR";
    }

    public static void main(String args[]) {
        ChiefComplaintGenerator gen = new ChiefComplaintGenerator();
        gen.run("src/main/resources/Scenario1_CCCIV","Scenario1_CCCIV.csv");
        gen.run("src/main/resources/Scenario1_CCMIL","Scenario1_CCMIL.csv");
    }
}
