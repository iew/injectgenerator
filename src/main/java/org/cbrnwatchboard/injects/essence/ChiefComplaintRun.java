package org.cbrnwatchboard.injects.essence;

import java.util.Date;
import java.util.List;

/**
 * The config data for one day of data imports for one ESSENCE machine
 * Created by gdonarum on 8/23/2016.
 */
public class ChiefComplaintRun {

    private String type;    // ex. CCMIL
    private int count;
    private List<Complaint> complaints;
    private Date date;
    private int percentMale;
    private int minAge;
    private int maxAge;
    private List<Zipcode> zipcodes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Complaint> getComplaints() {
        return complaints;
    }

    public void setComplaints(List<Complaint> complaints) {
        this.complaints = complaints;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getPercentMale() {
        return percentMale;
    }

    public void setPercentMale(int percentMale) {
        this.percentMale = percentMale;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public List<Zipcode> getZipcodes() {
        return zipcodes;
    }

    public void setZipcodes(List<Zipcode> zipcodes) {
        this.zipcodes = zipcodes;
    }

}
