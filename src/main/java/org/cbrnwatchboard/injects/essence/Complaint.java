package org.cbrnwatchboard.injects.essence;

/**
 * Created by gdonarum on 8/24/2016.
 */
public class Complaint {
    private String text;
    private int distribution;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getDistribution() {
        return distribution;
    }

    public void setDistribution(int distribution) {
        this.distribution = distribution;
    }
}
