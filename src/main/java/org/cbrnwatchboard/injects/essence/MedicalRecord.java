package org.cbrnwatchboard.injects.essence;

import java.util.Date;

/**
 * Created by gdonarum on 8/23/2016.
 */
public class MedicalRecord {
    private String rowId;
    private Date date;
    private Date lastModifiedDate;
    private String sendingFacility;
    private String hospital;
    private String facilityId;
    private String medRecNo;
    private String chiefComplaintOrig;
    private int age;
    private Date messageDateTime;
    private Date time;
    private String zipCode;
    private String sex;

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getSendingFacility() {
        return sendingFacility;
    }

    public void setSendingFacility(String sendingFacility) {
        this.sendingFacility = sendingFacility;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public String getMedRecNo() {
        return medRecNo;
    }

    public void setMedRecNo(String medRecNo) {
        this.medRecNo = medRecNo;
    }

    public String getChiefComplaintOrig() {
        return chiefComplaintOrig;
    }

    public void setChiefComplaintOrig(String chiefComplaintOrig) {
        this.chiefComplaintOrig = chiefComplaintOrig;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getMessageDateTime() {
        return messageDateTime;
    }

    public void setMessageDateTime(Date messageDateTime) {
        this.messageDateTime = messageDateTime;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
