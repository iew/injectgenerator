package org.cbrnwatchboard.injects.essence;

/**
 * Created by gdonarum on 8/24/2016.
 */
public class Zipcode {
    private String code;
    private int distribution;
    private String hospital;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getDistribution() {
        return distribution;
    }

    public void setDistribution(int distribution) {
        this.distribution = distribution;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }
}
